# README #

Un proiect construit cu symfony, unde vrem sa validam un string daca poate fi un CNP.

### Unde se afla functionalitatea ###

Pentru a folosi functia de validare, vom accesa route-ul /validator/{cnp}, unde inlocuim {cnp} cu sirul de caractere pe care vrem sa il testam.

### Cum pornim proiectul ###

Proiectul va rula pe localhost dupa comanda "symfony server:start"

### Proiect realizat de Victor Marin, in data dde 10.09.2021 ###